/*
 * PageFlipperActivity
 * Copyright (c) 2015 Adflecto. All Rights Reserved.
 */
package ru.adflecto.vastdemo;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.adflecto.sdk.Adflecto;
import ru.adflecto.sdk.Demand;
import ru.adflecto.sdk.events.AdflectoAdListener;
import ru.adflecto.sdk.nativead.AdflectoNativeView;
import ru.adflecto.sdk.ui.SwipeDirection;
import ru.adflecto.sdk.util.Logger;

/**
 * This is a demo class that show an example of adflecto native video ad format using as standalone {@link View} with
 * {@link AdflectoNativeView}
 */
public class PageFlipperActivity extends AppCompatActivity {

    /**
     * Logger's module identifier
     */
    private static final String TAG = "PageFlipperActivity";

    private boolean videoShown = false;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    /** A map that holds created ad block fragments per ad position */
    private Map<Integer, WeakReference<AdBlockFragment>> adBlocks = new HashMap<>();

    /** A variable that holds previously selected fragment's position. It is used to determine a
     * direction of page flipping
     */
    private int prevSelected = -1;

    /** Holds current flipping direction */
    private Direction direction = Direction.FORWARD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flipper);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity starting with 1
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (position > prevSelected) {
                    direction = Direction.FORWARD;
                } else {
                    direction = Direction.BACKWARD;
                }
                prevSelected = position;

                if (position % 3 == 1) {
                    AdBlockFragment fragment = adBlocks.get(position).get();
                    AdflectoNativeView anv = fragment.getAdView();
                    if (!anv.isAdReady()) {
                        fragment.getNoAdImage().setVisibility(View.VISIBLE);
                        anv.loadAd();
                    } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    }
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        String images [] = {"1", "2", "3", "4", "5", "6", "7"};

        List<String> newsIds = new ArrayList<>(Arrays.asList(images));

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_PAGE_NUMBER = "page_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int pageNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_PAGE_NUMBER, pageNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            int pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
            int originalPosition = getOriginalPosition(pageNumber);

            LinearLayout rootView = (LinearLayout) inflater.inflate(R.layout.fragment_text, container, false);

            String id = "_empty";
            if (originalPosition < newsIds.size()) {
                id = newsIds.get(originalPosition);
            }


            ImageView newsImage = (ImageView) rootView.findViewById(R.id.news_img);
            newsImage.setImageResource(getResources().getIdentifier("news_img" + id, "drawable", getContext().getPackageName()));

            TextView newsTitle = (TextView) rootView.findViewById(R.id.news_title);
            newsTitle.setText(getResources().getString(getResources().getIdentifier("news" + id + "_title", "string", getContext().getPackageName())));


            TextView newsBody = (TextView) rootView.findViewById(R.id.news_body);
            newsBody.setMovementMethod(new ScrollingMovementMethod());
            newsBody.setText(getResources().getString(getResources().getIdentifier("news" + id + "_body", "string", getContext().getPackageName())));

            return rootView;
        }


        /**
         * Returns an original position of the news item in the list, i.e. what the position will be
         * if all ad blocks will be removed from the list
         * @param currentPositionNumber
         *
         * @return a position number of the news item in the list without ad blocks
         */
        private int getOriginalPosition(int currentPositionNumber) {
            return currentPositionNumber - (currentPositionNumber / 3 + (currentPositionNumber % 3 > 1 ? 1 : 0));
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position % 3 == 1) {
                AdBlockFragment atf = new AdBlockFragment();
                atf.setListener(new AdflectoAdListener() {
                    @Override
                    public void onMediaLoading(Demand demand, String message) {
                        Logger.d(TAG, demand.getSource() + ". Media still loading: " + message);
                    }

                    @Override
                    public void onNoFill(Demand demand) {
                        String message = "\"NO BANNER\" response received from " + demand.getSource();
                        Logger.i(TAG, message);
                    }

                    @Override
                    public void onPostponed(Demand demand) {
                        String msg = "An ad from " + demand.getSource() + " was postponed and being loaded in background mode";
                        Logger.i(TAG, msg);
                    }

                    @Override
                    public void onReady(Demand demand) {
                        Logger.i(TAG, "An ad from " + demand.getSource() + " has been successfully loaded and put into cache");
                        videoShown = false;
                        for(WeakReference<AdBlockFragment> fragment: adBlocks.values()) {
                            fragment.get().getNoAdImage().setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onNotShow(Demand demand, String message) {
                        Logger.e(TAG, "Unable to show ad because of Error: " + demand.getErrorType());
                    }

                    @Override
                    public void onClosed(Demand demand) {
                        Logger.i(TAG, "Ad was closed");

                        if (!videoShown) {
                            moveToNextPage();
                            videoShown = true;
                        }
                    }

                    @Override
                    public void onShown(Demand demand) {
                        String message = "";
                        switch (demand.getStatus()) {
                            case CLICK: {
                                message = "Ad was clicked";
                                break;
                            }
                            case COMPLETE: {
                                message = "Ad was shown until the end. Hope you enjoyed it!";
                                break;
                            }
                        }

                        if (!videoShown) {
                            moveToNextPage();
                            videoShown = true;
                        }

                        Logger.i(TAG, message);
                    }

                    @Override
                    public void onSwipe(SwipeDirection direction) {

                    }
                });
                adBlocks.put(position, new WeakReference<AdBlockFragment>(atf));
                return atf;
            }
            return PlaceholderFragment.newInstance(position);
        }

        private void moveToNextPage() {
            // Определяем страницу на которую нужно переключиться после того как рекламный ролик закончился. Страница
            // определяется в зависимости от того, листали мы вперед или назад. Если ролик показывался на первой или последней странице
            // переходим на вторую и предпоследюнюю соответственно.
            // Важно, поскольку мы используем разные instance AdflectoNativeView и AdflectoAdListener, метод onShown будет вызван для всех
            // AdflectoAdListener, поэьлму мы используем дополнительный флаг videoShown, чтобы переключить страницу только один раз
            int newPage = direction == Direction.FORWARD ? prevSelected + 1 : prevSelected - 1;
            if (newPage < 0) {
                newPage = 1;
                prevSelected = 1;
            } else if (newPage > getCount() - 1) {
                newPage = getCount() - 2;
                prevSelected = getCount() - 2;
            }
            mViewPager.setCurrentItem(newPage, true);
        }


        @Override
        public int getCount() {
            return 15;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "SECTION " + position;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Adflecto.onBackPressed(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Adflecto.onDestroy(this);
        mViewPager.removeAllViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Adflecto.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Adflecto.onResume(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Adflecto.onStop(this);
    }
}
