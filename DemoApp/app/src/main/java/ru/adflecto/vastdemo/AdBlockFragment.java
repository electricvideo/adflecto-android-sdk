/*
 * AdBlockFragment
 * Copyright (c) 2016 Adflecto. All Rights Reserved.
 */
package ru.adflecto.vastdemo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import ru.adflecto.sdk.events.AdflectoAdListener;
import ru.adflecto.sdk.nativead.AdflectoNativeView;
import ru.adflecto.sdk.util.Logger;

public class AdBlockFragment extends Fragment {

    /** Logger's module identifier */
    public static final String TAG = "AdBlockFragment";

    /** Ad block container instance*/
    private AdflectoNativeView mAdflectoNativeView = null;

    /** Ad block container events' listener */
    private AdflectoAdListener listener;

    /** Ad player holder container */
    private ViewGroup adContainer;

    /** Stub image that is hsown in case of no ad */
    ImageView noAdImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.v(TAG, "AdBlockFragment is created");
        adContainer = (ViewGroup) inflater.inflate(R.layout.ad_block_container, container, false);
        adContainer.setBackgroundColor(Color.BLACK);
        adContainer.setVisibility(View.VISIBLE);

        noAdImage = (ImageView) adContainer.findViewById(R.id.ad_stub);
        AdflectoNativeView adView = getAdView();
        if (adView.isAdReady()) {
            noAdImage.setVisibility(View.INVISIBLE);
        } else {
            noAdImage.setVisibility(View.VISIBLE);
        }
        adContainer.addView(adView);

        return adContainer;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        adContainer.removeAllViews();
        adContainer = null;
        mAdflectoNativeView = null;

        Logger.v(TAG, "AdBlockFragment is destroyed");
    }

    public void setListener(AdflectoAdListener listener) {
        this.listener = listener;
    }

    public AdflectoNativeView getAdView() {
        if (mAdflectoNativeView == null) {
            mAdflectoNativeView = new AdflectoNativeView(
                    getActivity(),
                    "53",
                    listener,
                    true
            );
            mAdflectoNativeView.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );

            params.addRule(RelativeLayout.CENTER_IN_PARENT);

            mAdflectoNativeView.setLayoutParams(params);
            mAdflectoNativeView.loadAd();
        }
        return mAdflectoNativeView;
    }

    public ImageView getNoAdImage() {
        return noAdImage;
    }
}
