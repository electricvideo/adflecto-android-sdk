/*
 * DemoActivity
 * Copyright (c) 2015 Adflecto. All Rights Reserved.
 */
package ru.adflecto.vastdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import ru.adflecto.sdk.Adflecto;
import ru.adflecto.sdk.AdflectoMediator;
import ru.adflecto.sdk.Demand;
import ru.adflecto.sdk.DemandSource;
import ru.adflecto.sdk.Format;
import ru.adflecto.sdk.events.AdflectoAdListener;
import ru.adflecto.sdk.ui.SwipeDirection;
import ru.adflecto.sdk.util.Logger;

//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;

/**
 * This is a demo class that show an example of integration with Adflecto Android Ad module.
 */
public class DemoActivity extends Activity implements AdflectoAdListener {

    /** Logger's module identifier */
    private static final String TAG = "DemoActivity";

    TextView consoleView;
    TextView aboutView;
    TextView deviceId;
    ProgressBar progressBar;

    AdflectoMediator mediationFormat;
    AdflectoMediator customLinkFormat;
    AdflectoMediator fullScreenFormat;

    //buttons
    Button magazineBtn;
    Button fullScreenBtn;
    Button customLinkBtn;
    Button mediationBtn;
    Button nativeListBtn;

    Map<String,Button> uiButtons = new HashMap<>();

    Map<String,Boolean> mediationButtons = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Adflecto.initialize(this);
        setContentView(R.layout.activity_demo);

//        consoleView = (TextView) findViewById(R.id.view_result_box);
        consoleView = new TextView(this);
        consoleView.setMovementMethod(new ScrollingMovementMethod());
        String buildNumber = Adflecto.getBuildNumber();
        aboutView = (TextView) findViewById(R.id.about_textView);
        aboutView.setText(Html.fromHtml(aboutView.getText().toString()));
        deviceId = (TextView) findViewById(R.id.device_id);
        deviceId.setText(Adflecto.getDeviceId() + " (# " + buildNumber + ")");

        progressBar = (ProgressBar) findViewById(R.id.ad_load_progress);
        progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.my_progress));
        progressBar.setMax(100);

        //buttons
        magazineBtn = (Button) findViewById(R.id.btn_magazine);

        fullScreenBtn = (Button) findViewById(R.id.btn_fullscreen);
        fullScreenBtn.setEnabled(false);

        customLinkBtn = (Button) findViewById(R.id.btn_custom_link);
        customLinkBtn.setEnabled(false);

        mediationBtn = (Button) findViewById(R.id.btn_mediation);
        mediationBtn.setEnabled(false);

        nativeListBtn = (Button) findViewById(R.id.btn_native_list);


        fullScreenFormat = new AdflectoMediator(this, Format.INTERSTITIAL, this);
        fullScreenFormat.addDemandSource(DemandSource.ADFLECTO_NETWORK, "51");
        uiButtons.put("51", fullScreenBtn);


        mediationFormat = new AdflectoMediator(this, Format.INTERSTITIAL, this);
        mediationFormat.addDemandSource(DemandSource.ADFLECTO_NETWORK, "50");
        mediationFormat.addDemandSource(DemandSource.MOPUB, "ec7c4b7b8ece473e802ddff002ee291b", new BigDecimal("0.18"), BigDecimal.ZERO); //ec7c4b7b8ece473e802ddff002ee291b
        mediationFormat.addDemandSource(DemandSource.MOPUB, "2dd61bd62471478a9ef767346ef3dd09"); //ec7c4b7b8ece473e802ddff002ee291b

        uiButtons.put("50", mediationBtn);
        uiButtons.put("2dd61bd62471478a9ef767346ef3dd09", mediationBtn); //ec7c4b7b8ece473e802ddff002ee291b
        uiButtons.put("ec7c4b7b8ece473e802ddff002ee291b", mediationBtn); //ec7c4b7b8ece473e802ddff002ee291b

        mediationButtons.put("50", false);
        mediationButtons.put("2dd61bd62471478a9ef767346ef3dd09", false); //ec7c4b7b8ece473e802ddff002ee291b
        mediationButtons.put("ec7c4b7b8ece473e802ddff002ee291b", false); //ec7c4b7b8ece473e802ddff002ee291b

        //custom link
        customLinkFormat = new AdflectoMediator(this, Format.INTERSTITIAL, this);
        customLinkFormat.addDemandSource(DemandSource.ADFLECTO_NETWORK, "47");
        uiButtons.put("47", customLinkBtn);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_cache:
                String result = Adflecto.clearMediaCache();
                consoleView.append(result);
                consoleView.append(System.getProperty("line.separator"));
                consoleView.append(System.getProperty("line.separator"));
                progressBar.setProgress(0);
                fullScreenBtn.setEnabled(false);
                customLinkBtn.setEnabled(false);
                mediationBtn.setEnabled(false);
                return true;
            case R.id.send_logs:
                Logger.processLogSending();
                consoleView.append("Logs was sent to server");
                consoleView.append(System.getProperty("line.separator"));
                consoleView.append(System.getProperty("line.separator"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void showFullscreenAd(View view) {
        fullScreenFormat.show();

    }

    public void showCustomLinkPanel(View view) {
        customLinkFormat.show();
    }

    public void showMediation(View view) {
        mediationFormat.show();
    }

    public void loadAllAds(View view) {
        boolean keepProgress = true;

        keepProgress &= mediationFormat.loadAds();

        keepProgress &= fullScreenFormat.loadAds();
        keepProgress &= customLinkFormat.loadAds();
        if(!keepProgress) {
            progressBar.setProgress(0);
        }
    }


    public void nativeMagazineExample(View view) {
        Intent pageFlipperIntent = new Intent( this, PageFlipperActivity.class);
        startActivity(pageFlipperIntent);
    }


    public void nativeListExample(View view) {
        Intent analyticIntent = new Intent( this, NativeDemoActivity.class);
        startActivity(analyticIntent);
    }


    @Override
    public void onMediaLoading(Demand demand, final String message) {
        Logger.d(TAG, demand.getSource() + ". Media is still loading: " + message);
        progressBar.incrementProgressBy(10);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                consoleView.append(message + "\n");
            }
        });
    }


    @Override
    public void onReady(Demand demand) {
        Logger.i(TAG, "An ad from " + demand.getSource() + " has been successfully loaded and put into cache");
        consoleView.append("An ad from " + demand.getSource() + " has been successfully loaded and put into cache");
        consoleView.append(System.getProperty("line.separator"));
        consoleView.append(System.getProperty("line.separator"));

        progressBar.setProgress(100);

        if (mediationButtons.containsKey(demand.getAdUnitId())) {
            mediationButtons.put(demand.getAdUnitId(), true);
        }

        uiButtons.get(demand.getAdUnitId()).setEnabled(true);
    }


    @Override
    public void onNoFill(Demand demand) {
        String message = "\"NO BANNER\" response received from " + demand.getSource();
        Logger.i(TAG, message);
        consoleView.append(message);
        consoleView.append(System.getProperty("line.separator"));
        consoleView.append(System.getProperty("line.separator"));
    }


    @Override
    public void onPostponed(Demand demand) {
       String msg = "An ad from " + demand.getSource() + " was postponed and being loaded in background mode";
       Logger.i(TAG, msg);
       consoleView.append(msg);
       consoleView.append(System.getProperty("line.separator"));
       consoleView.append(System.getProperty("line.separator"));
    }


    @Override
    public void onNotShow(Demand demand, final String message) {
        Logger.e(TAG, "Unable to show ad because of Error: " + demand.getErrorType());
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                consoleView.append(message);
                consoleView.append(System.getProperty("line.separator"));
                consoleView.append(System.getProperty("line.separator"));
            }
        });
    }


    @Override
    public void onClosed(Demand demand) {
        Logger.i(TAG, "An ad from " + demand.getSource() + " was closed");
        consoleView.append("An ad from " + demand.getSource() + " was closed");
        consoleView.append(System.getProperty("line.separator"));
        consoleView.append(System.getProperty("line.separator"));

        boolean enabledStatus = false;

        if (mediationButtons.containsKey(demand.getAdUnitId())) {
            mediationButtons.put(demand.getAdUnitId(), false);

            for(Boolean status: mediationButtons.values()) {
                enabledStatus |= status;
            }
        }
        uiButtons.get(demand.getAdUnitId()).setEnabled(enabledStatus);
    }


    @Override
    public void onShown(Demand demand) {
        String message = "";
        switch (demand.getStatus()) {
            case CLICK: {
                message = "Ad was clicked";
                break;
            }
            case COMPLETE: {
                message = "Ad was shown until the end. Hope you enjoyed it!";
                break;
            }
        }

        Logger.i(TAG, message);
        consoleView.append(message);
        consoleView.append(System.getProperty("line.separator"));
        consoleView.append(System.getProperty("line.separator"));

        boolean enabledStatus = false;

        if (mediationButtons.containsKey(demand.getAdUnitId())) {
            mediationButtons.put(demand.getAdUnitId(), false);

            for(Boolean status: mediationButtons.values()) {
                enabledStatus |= status;
            }
        }
        uiButtons.get(demand.getAdUnitId()).setEnabled(enabledStatus);
    }

    @Override
    public void onSwipe(SwipeDirection direction) {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Adflecto.onDestroy(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Adflecto.onStop(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Adflecto.onRestart(this);
    }
}