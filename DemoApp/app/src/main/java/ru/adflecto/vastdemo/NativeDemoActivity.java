/*
 * NativeDemoActivity
 * Copyright (c) 2015 Adflecto. All Rights Reserved.
 */
package ru.adflecto.vastdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import ru.adflecto.sdk.Adflecto;
import ru.adflecto.sdk.events.AdflectoAdListener;
import ru.adflecto.sdk.Demand;
import ru.adflecto.sdk.nativead.AdflectoNativeVideoAdAdapter;
import ru.adflecto.sdk.nativead.NativeVideoLayoutBinder;
import ru.adflecto.sdk.ui.SwipeDirection;
import ru.adflecto.sdk.util.Logger;

/**
 * This is a demo class that show an example of adflecto native video ad format using inside Android {@link ListView}
 * with {@link AdflectoNativeVideoAdAdapter}
 */
public class NativeDemoActivity extends Activity implements AdflectoAdListener {

    /** Logger's module identifier */
    private static final String TAG = "NativeDemoActivity";


    private TextView adStatus;

    ListView goodsList;

    final String ATTRIBUTE_NAME_TITLE = "title";
    final String ATTRIBUTE_NAME_DATE = "date";


    String headers [] = {
            "Аппарат mars insight не полетит на марс в следующем году",
            "Астрономы поймали пять таинственных сигналов из космоса",
            "NASA хочет использовать бактерии в качестве источника энергии в космосе",
            "Китай и США создают «космическую горячую линию» для избежания конфликтов",
            "NASA начало выращивать цветы на мкс",
            "Как будет выглядеть следующий китайский космический корабль?",
            "«Космический стакан» позволит пить виски в условиях невесомости",
            "Немецкое аэрокосмическое агентство собирается создать суборбитальный пассажирский самолёт к 2030 году",
            "Астронавт крис хэдфилд готовится выпустить записанный в космосе альбом",
            "Yotaphone переезжает на новую операционную систему",
            "Samsung представила 64-битный процессор exynos 7 octa",
            "В сеть утекли изображения «убийцы tesla» faraday future",
            "Почему мир пока не готов к самоуправляемым автомобилям?",
            "Aston martin планирует выпустить 800-сильный электромобиль",
            "Глава uber хочет купить 500 000 самоуправляемых автомобилей tesla",
            "Mercedes следует примеру tesla и выпускает домашние накопители энергии",
            "В японии началось строительство плавающей солнечной электростанции",
            "Первая линия высокоскоростной транспортной системы hyperloop откроется в 2018 году",
            "Созданы крошечные имплантаты для сканирования мозга",
            "Человечество вдвое ускорило потепление мирового океана",
            "Windows 10 стала второй самой популярной ос в мире",
            "Компания audi представила свой луноход lunar quattro",
            "Россия планирует построить ядерный космолёт к 2025 году",
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_native);

        LinearLayout statusPanel = (LinearLayout) findViewById(R.id.statusPanel);
        statusPanel.setVisibility(View.GONE);
        this.adStatus = (TextView) findViewById(R.id.adStatus);
        this.goodsList = (ListView) findViewById(R.id.goodsList);

        prepareNativeVideo(goodsList);
    }



    private void prepareNativeVideo(ListView listView) {
        String[] from = {ATTRIBUTE_NAME_TITLE, ATTRIBUTE_NAME_DATE};
        int[] to = { R.id.itemHeader, R.id.itemPrice};

        NativeVideoLayoutBinder layoutBinder = new NativeVideoLayoutBinder(
                R.layout.native_video_ad_item,
                R.id.videoContainer);

        AdflectoNativeVideoAdAdapter newAdAdapter = new AdflectoNativeVideoAdAdapter(
                this,
                "52",
                10,
                7,
                new SimpleAdapter(this, generateGoodsList(), R.layout.gooditem, from, to),
                layoutBinder,
                this
        );

        newAdAdapter.load();

        listView.setAdapter(newAdAdapter);
    }


    private List <Map<String, Object>> generateGoodsList() {
        ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>();

        Random rnd = new Random();
        Map<String, Object> m;
        Calendar cal = Calendar.getInstance();
        for (int i = 0; i < 20; i++) {
            int selected = rnd.nextInt(headers.length);

            int dayShift = rnd.nextInt(3);
            cal.add(Calendar.DATE, -1 * dayShift);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM", new Locale("ru", "RU"));

            m = new HashMap<>();
            m.put(ATTRIBUTE_NAME_TITLE, headers[selected]);
            m.put(ATTRIBUTE_NAME_DATE, sdf.format(cal.getTime()));
            data.add(m);
        }

        return data;
    }


    @Override
    public void onMediaLoading(Demand demand, final String message) {
        Logger.d(TAG, demand.getSource() + ". Media still loading: " + message);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adStatus.setText(message + "\n");
            }
        });
    }


    @Override
    public void onReady(Demand demand) {
        Logger.i(TAG, "An ad from " + demand.getSource() + " has been successfully loaded and put into cache");
        adStatus.setText("An ad from " + demand.getSource() + " has been successfully loaded and put into cache");
    }


    @Override
    public void onNoFill(Demand demand) {
        String message = "\"NO BANNER\" response received from " + demand.getSource();
        Logger.i(TAG, message);
        adStatus.setText(message);
    }


    @Override
    public void onPostponed(Demand demand) {
        String msg = "An ad from " + demand.getSource() + " was postponed and being loaded in background mode";
        Logger.i(TAG, msg);
        adStatus.setText(msg);
    }


    @Override
    public void onNotShow(Demand demand, final String message) {
        Logger.e(TAG, "Unable to show ad because of Error: " + demand.getErrorType());
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adStatus.setText(message);
            }
        });
    }


    @Override
    public void onClosed(Demand demand) {
        Logger.i(TAG, "Ad was closed");
        adStatus.setText("An ad from " + demand.getSource() + " was closed");
    }


    @Override
    public void onShown(Demand demand) {
        String message = "";
        switch (demand.getStatus()) {
            case CLICK: {
                message = "Ad was clicked";
                break;
            }
            case COMPLETE: {
                message = "Ad was shown until the end. Hope you enjoyed it!";
                break;
            }
        }

        Logger.i(TAG, message);
        adStatus.setText(message);
    }

    @Override
    public void onSwipe(SwipeDirection direction) {
        Logger.i(TAG, "Ad was swipped: " + direction);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Adflecto.onDestroy(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Adflecto.onStop(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Adflecto.onRestart(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Adflecto.onBackPressed(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Adflecto.onPause(this);
    }
}
