*adflecto* Android SDK
==================================
Modified: May 11, 2016
SDK Version: 2.11

To Download:
----------------------------------
Please go to the [Downloads](https://bitbucket.org/adflecto/adflecto-android-sdk/downloads) section and press "**Download Repository**" button or use 

```
git clone https://artemvolftrub@bitbucket.org/adflecto/adflecto-android-sdk.git

```
to obtain the latest version of adflecto SDK for Android platform.

Contains:
----------------------------------
* DemoApp - source code of a simple application that shows how to integrate with Adflecto SDK
* Library - latest version of adflecto SDK
* ExampleApk - two applications demonstrating SDK functionality. Simply install them on your device and try out experience for yourself


Getting Started with SDK:
----------------------------------
All users are recommended to review this [documentation](https://bitbucket.org/adflecto/adflecto-android-sdk/wiki/Home) that describes how to add adflecto SDK into your applictions.


Legal Requirements:
----------------------------------
By downloading adflecto SDK, you consent to comply with limited, non-commercial license to use and review the SDK - solely for evaluation purposes. If you wish to integrate this SDK into any commercial application, you must contact adflecto via email [publishers@adflecto.ru](mailto:publishers@adflecto.ru) to sign adflecto publisher agreement and become registered adflecto publisher.

Contact Us:
----------------------------------
For more information, please visit [adflecto.ru](http://adflecto.ru). In case of any question, please email us at [publishers@adflecto.ru](mailto:publishers@adflecto.ru)